Param(
  [string]$server = "testserver.helloworldopen.com",
  [string]$port = "8091"
)
. ./config-win.ps1

Push-Location -Path $pl
./run-win.ps1 $server $port $botname $botkey

Write-Host "Press any key to continue ..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")