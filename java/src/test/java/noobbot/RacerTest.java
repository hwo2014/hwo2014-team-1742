package noobbot;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import org.junit.rules.ErrorCollector;

import noobbot.messages.*;
import noobbot.messages.serialization.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Kendrick Boyd on 4/18/14.
 */
public class RacerTest {
    final Gson gson = HelloWordOpenGson.MakeGson();

    final Join join = new Join("thename", "akey");

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test
    public void makeRacerNormal() {
        CarID carId = gson.fromJson("{\"name\": \"guinea pig\", \"color\": \"blue\"}", CarID.class);
        Racer racer = Racer.makeRacer(carId, join);

        collector.checkThat(racer, notNullValue());
        collector.checkThat(racer.getId(), notNullValue());
        collector.checkThat(racer.getId().name, is("guinea pig"));
        collector.checkThat(racer.getId().color, is("blue"));
    }

    @Test
    public void makeRacerAbnormal() {
        CarID carId = gson.fromJson("{\"data\": \"invalid\"}", CarID.class);
        Racer racer = Racer.makeRacer(carId, join);

        collector.checkThat(racer, notNullValue());
        collector.checkThat(racer.getId(), notNullValue());
        collector.checkThat(racer.getId().name, is("thename"));
        collector.checkThat(racer.getId().color, is("red"));
    }
}
