package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import noobbot.messages.BotId;
import noobbot.messages.Car;
import noobbot.messages.CarDimensions;
import noobbot.messages.CarID;
import noobbot.messages.CreateRace;
import noobbot.messages.GameInit;
import noobbot.messages.Join;
import noobbot.messages.JoinRace;
import noobbot.messages.Lane;
import noobbot.messages.RaceSession;
import noobbot.messages.RadiusTrackPiece;
import noobbot.messages.StartingPoint;
import noobbot.messages.TrackPiece;
import noobbot.messages.serialization.HelloWordOpenGson;
import noobbot.messages.serialization.MessageDeserializer;
import noobbot.messages.MsgWrapper;
import noobbot.messages.serialization.TrackPieceDeserializer;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.rules.ErrorCollector;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.List;

/**
 * Created by gblike on 4/15/14.
 */
public class MessageParserTest {
    final Gson gson = HelloWordOpenGson.MakeGson();
    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test
    public void exportJoinRaceMessage() throws Exception {
        String expected = "{\"msgType\": \"joinRace\", \"data\": {" +
                             "\"botId\": {" +
                                "\"name\": \"keke\"," +
                                "\"key\": \"IVMNERKWEW\"" +
                             "}," +
                             "\"trackName\": \"hockenheim\"," +
                             "\"password\": \"schumi4ever\"," +
                             "\"carCount\": 3" +
                          "}}";


        JoinRace joinRace = new JoinRace(new BotId("keke", "IVMNERKWEW"),"hockenheim", "schumi4ever", 3);
        String actual = joinRace.toJson();
        JSONAssert.assertEquals(expected, actual, false);
    }

    @Test
    public void exportCreateRaceMessage() throws Exception {
        String expected = "{\"msgType\": \"createRace\", \"data\": {" +
                            "\"botId\": {" +
                                "\"name\": \"schumacher\"," +
                                "\"key\": \"UEWJBVNHDS\"" +
                            "}," +
                            "\"trackName\": \"hockenheim\"," +
                            "\"password\": \"schumi4ever\"," +
                            "\"carCount\": 3" +
                           "}}";


        CreateRace createRace = new CreateRace(new BotId("schumacher", "UEWJBVNHDS"),"hockenheim", "schumi4ever", 3);
        String actual = createRace.toJson();
        JSONAssert.assertEquals(expected, actual, false);
    }

    @Test
    public void readGameInitMessage() throws Exception {
        // This is kind of a monolithic test of all the components of this message.
        // This can be broken down into its component parts.

        String serverMessage = "{\"msgType\": \"gameInit\", \"data\": {" +
                                    "\"race\": {" +
                                        "\"track\": {" +
                                            "\"id\": \"indianapolis\"," +
                                                    "\"name\": \"Indianapolis\"," +
                                                    "\"pieces\": [" +
                                                        "{" +
                                                            "\"length\": 100.0" +
                                                        "}," +
                                                        "{" +
                                                            "\"length\": 100.0," +
                                                            "\"switch\": true" +
                                                        "}," +
                                                        "{" +
                                                            "\"radius\": 200," +
                                                            "\"angle\": 22.5" +
                                                        "}" +
                                                    "]," +
                                                    "\"lanes\": [" +
                                                        "{" +
                                                            "\"distanceFromCenter\": -20," +
                                                            "\"index\": 0" +
                                                        "}," +
                                                        "{" +
                                                            "\"distanceFromCenter\": 0," +
                                                            "\"index\": 1" +
                                                        "}," +
                                                        "{" +
                                                            "\"distanceFromCenter\": 20," +
                                                            "\"index\": 2" +
                                                        "}" +
                                                    "]," +
                                                    "\"startingPoint\": {" +
                                                        "\"position\": {" +
                                                            "\"x\": -340.0," +
                                                            "\"y\": -96.0" +
                                                        "}," +
                                                        "\"angle\": 90.0" +
                                                    "}" +
                                                "}," +
                                        "\"cars\": [" +
                                            "{" +
                                                "\"id\": {" +
                                                    "\"name\": \"Schumacher\"," +
                                                    "\"color\": \"red\"" +
                                                "}," +
                                                 "\"dimensions\": {" +
                                                    "\"length\": 40.0," +
                                                    "\"width\": 20.0," +
                                                    "\"guideFlagPosition\": 10.0" +
                                                "}" +
                                            "}," +
                                            "{" +
                                                "\"id\": {" +
                                                    "\"name\": \"Rosberg\"," +
                                                    "\"color\": \"blue\"" +
                                                "}," +
                                                "\"dimensions\": {" +
                                                    "\"length\": 40.0," +
                                                    "\"width\": 20.0," +
                                                    "\"guideFlagPosition\": 10.0" +
                                                "}" +
                                            "}" +
                                        "]," +
                                        "\"raceSession\": {" +
                                            "\"laps\": 3," +
                                            "\"maxLapTimeMs\": 30000," +
                                            "\"quickRace\": true" +
                                        "}" +
                                    "}" +
                                "}}";


        MsgWrapper msgWrapper = gson.fromJson(serverMessage, MsgWrapper.class);
        collector.checkThat(msgWrapper, notNullValue());
        collector.checkThat(msgWrapper.data, notNullValue());
        collector.checkThat(msgWrapper.data, instanceOf(GameInit.class));

        GameInit gameInit = (GameInit) msgWrapper.data;

        // Make sure all the children got the values we expect
        collector.checkThat(gameInit.race, notNullValue());
        collector.checkThat(gameInit.race.track, notNullValue());
        collector.checkThat(gameInit.race.track.id, notNullValue());
        collector.checkThat(gameInit.race.track.name, notNullValue());

        // Pieces
        List<TrackPiece> pieces = gameInit.race.track.pieces;
        collector.checkThat(pieces, allOf(notNullValue(), instanceOf(List.class), is(not(empty()))));
        collector.checkThat(pieces.size(), is(3));
        collector.checkThat(pieces.get(0), instanceOf(TrackPiece.class)); // regular straight track
        collector.checkThat(pieces.get(0).hasSwitch, is(false)); // regular straight track
        collector.checkThat(pieces.get(1), instanceOf(TrackPiece.class)); // regular switch track
        collector.checkThat(pieces.get(1).hasSwitch, is(true)); // regular switch track
        collector.checkThat(pieces.get(2), instanceOf(RadiusTrackPiece.class)); // curved track track

        // Lanes
        List<Lane> lanes = gameInit.race.track.lanes;
        collector.checkThat(lanes, allOf(notNullValue(), instanceOf(List.class), is(not(empty()))));
        collector.checkThat(lanes.size(), is(3));
        collector.checkThat(lanes.get(0).distanceFromCenter, is(-20));
        collector.checkThat(lanes.get(0).index, is(0));

        // Starting Position
        StartingPoint startingPoint = gameInit.race.track.startingPoint;
        collector.checkThat(startingPoint, notNullValue());
        collector.checkThat(startingPoint.angle, is(90.0f));
        collector.checkThat(startingPoint.position.x, is(-340.0f));
        collector.checkThat(startingPoint.position.y, is(-96.0f));
        collector.checkThat(startingPoint, notNullValue());

        // Cars
        List<Car> cars = gameInit.race.cars;
        collector.checkThat(cars, allOf(notNullValue(), instanceOf(List.class), is(not(empty()))));
        CarID id = cars.get(0).id;
        collector.checkThat(id.name, is("Schumacher"));
        collector.checkThat(id.color, is("red"));

        CarDimensions dimensions = cars.get(0).dimensions;
        collector.checkThat(dimensions.length, is(40.0f));
        collector.checkThat(dimensions.width, is(20.0f));
        collector.checkThat(dimensions.guideFlagPosition, is(10.0f));

        // Race Session
        RaceSession raceSession = gameInit.race.raceSession;
        collector.checkThat(raceSession.laps, is(3));
        collector.checkThat(raceSession.maxLapTimeMs, is(30000));
        collector.checkThat(raceSession.quickRace, is(true));


    }
}
