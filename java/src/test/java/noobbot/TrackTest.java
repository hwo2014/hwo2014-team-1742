package noobbot;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import noobbot.messages.Lane;
import noobbot.messages.RadiusTrackPiece;
import noobbot.messages.StartingPoint;
import noobbot.messages.Track;
import noobbot.messages.TrackPiece;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by gblike on 4/19/14.
 */
public class TrackTest {
    @Test
    public void radiusLengthTest() {
        float radius = 100.0f;
        Track track = makeCircularTrack(radius, 4);
        final Lane lane = track.lanes.get(0);
        Double trackLength = track.pieces.stream().mapToDouble(piece -> piece.getLengthInLane(lane)).sum();
        assertThat(trackLength, is(closeTo(Math.PI * 2.0 * (radius + lane.distanceFromCenter), 0.0001)));
    }

    @Test
    public void nextPiecesTest() {
        Track track = makeCircularTrack(100.0f, 4);
        List<TrackPiece> pieces = track.nextNPieces(1, 2);
        assertThat(pieces.size(), is(2));
        assertThat(pieces.get(0), is(track.pieces.get(1)));
        assertThat(track.pieces.indexOf(pieces.get(1)), is(2));
    }

    /**
     * make a 4 piece circular track with two lanes and a radius specified
     */
    public Track makeCircularTrack(final float radius, final int numberOfLanes) {
        StartingPoint startingPoint = new StartingPoint(new Point2D.Float(0.0f, 0.0f), 0.0f);
        List<TrackPiece> pieces = IntStream.
            range(0, 4).
            mapToObj(index -> new RadiusTrackPiece(radius, 90.0f, false)).
            collect(Collectors.toList());

        List<Lane> lanes = IntStream.
            range(0, numberOfLanes).
            mapToObj(index -> new Lane(index * 20 - (numberOfLanes * 10), index)).
            collect(Collectors.toList());

        return new Track("id", "name", pieces, lanes, startingPoint);
    }
}
