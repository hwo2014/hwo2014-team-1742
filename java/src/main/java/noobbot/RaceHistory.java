package noobbot;

import java.util.ArrayList;

/**
 * Created by gblike on 4/20/14.
 */
// TODO replace with a sparse list implementation and implement interpolaton
public class RaceHistory extends ArrayList<RaceLogEntry> {
    public RaceLogEntry last() {
        return last(-1);
    }

    public RaceLogEntry last(int n) {
        return size() + n >= 0 ? get(size() + n) : null;
    }
}
