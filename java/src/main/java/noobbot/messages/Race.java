package noobbot.messages;

import java.util.List;

/**
 * Created by gblike on 4/16/14.
 */
public class Race {
    public final Track track;
    public final List<Car> cars;
    public final RaceSession raceSession;

    public Race(final Track track, final List<Car> cars, final RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
}
