package noobbot.messages;

/**
 * Created by drzewiec on 4/17/2014.
 */
public class LaneInfo {
    public final int startLaneIndex;
    public final int endLaneIndex;

    public LaneInfo(final int startLaneIndex, final int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }

    @Override
    public String toString() {
        return "start-lane: " + startLaneIndex + ", end-lane: " + endLaneIndex;
    }
}
