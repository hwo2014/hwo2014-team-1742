package noobbot.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by gblike on 4/16/14.
 */
public class Track {
    public final String id;
    public final String name;
    public final List<TrackPiece> pieces;
    public final List<Lane> lanes;
    public final StartingPoint startingPoint;

    public Track(final String id, final String name, final List<TrackPiece> pieces, final List<Lane> lanes, final StartingPoint startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }

    public List<TrackPiece> nextNPieces(final int startIndex, final int numberToFetch) {
        return IntStream.
                range(0, numberToFetch).
                mapToObj(index -> pieces.get((index + startIndex) % pieces.size())).
                collect(Collectors.toList());
    }
}
