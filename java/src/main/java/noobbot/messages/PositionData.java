package noobbot.messages;

public class PositionData {
    public final CarID id;
    public final double angle;
    public final Position piecePosition;

    public PositionData(final CarID id, final double angle, final Position piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    @Override
    public String toString() {
        return "ID: {" + id.toString() + "}, angle: " + angle + ", position: {" + piecePosition.toString() + "}";
    }
}