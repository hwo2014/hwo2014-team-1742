package noobbot.messages.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import noobbot.messages.MsgWrapper;
import noobbot.messages.TrackPiece;

/**
 * Created by gblike on 4/19/14.
 */
public interface HelloWordOpenGson {
    public static Gson MakeGson() {
        return new GsonBuilder().
                    registerTypeAdapter(MsgWrapper.class, new MessageDeserializer()).
                    registerTypeAdapter(TrackPiece.class, new TrackPieceDeserializer()).
                    create();
    }
}
