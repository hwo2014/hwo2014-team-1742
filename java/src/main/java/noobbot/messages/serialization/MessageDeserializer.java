package noobbot.messages.serialization;

import com.google.gson.*;
import noobbot.messages.GameInit;
import noobbot.messages.MsgWrapper;
import noobbot.messages.CarID;

import java.lang.reflect.Type;

/**
 * Created by gblike on 4/16/14.
 */
public class MessageDeserializer implements JsonDeserializer<MsgWrapper> {

    @Override
    public MsgWrapper deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String messageType = json.getAsJsonObject().get("msgType").getAsString();
        JsonElement data = json.getAsJsonObject().get("data");
        Object realData = null;
        if("gameInit".equals(messageType)) {
            realData = context.deserialize(data, GameInit.class);
        } else if ("yourCar".equals(messageType)) {
            realData = context.deserialize(data, CarID.class);
        }
        MsgWrapper wrapper = new MsgWrapper(messageType, realData);
        return wrapper;
    }
}
