package noobbot.messages.serialization;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import noobbot.messages.RadiusTrackPiece;
import noobbot.messages.TrackPiece;

import java.lang.reflect.Type;

/**
 * Created by gblike on 4/17/14.
 */
public class TrackPieceDeserializer implements JsonDeserializer<TrackPiece> {
    /**
     * Formats to account for
     * "pieces": [
     *    {
     *       "length": 100.0
     *    },
     *    {
     *       "length": 100.0,
     *       "switch": true
     *    },
     *    {
     *       "radius": 200,
     *       "angle": 22.5
     *    }
     * ]
     */

    @Override
    public TrackPiece deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject data = json.getAsJsonObject();

        final boolean hasSwitch = data.has("switch") ? data.get("switch").getAsBoolean() : false;

        if(data.has("length")) {
            final float length = data.get("length").getAsFloat();
            return new TrackPiece(length, hasSwitch);
        } else if(data.has("radius") && data.has("angle")) {
            final float radius = data.get("radius").getAsFloat();
            final float angle = data.get("angle").getAsFloat();
            return new RadiusTrackPiece(radius, angle, hasSwitch);
        } else {
            throw new JsonParseException("Unable to determine track piece type: " + json.toString());
        }
    }
}
