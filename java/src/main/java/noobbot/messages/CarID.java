package noobbot.messages;

public class CarID {
    public final String name;
    public final String color;

    public CarID (final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
        return "name: " + name + ", color: " + color;
    }

    public boolean equals(CarID other) {
        if (name.equals(other.name) && color.equals(other.color)) return true;
        else return false;
    }
}
