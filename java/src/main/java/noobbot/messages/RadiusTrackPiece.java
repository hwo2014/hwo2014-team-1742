package noobbot.messages;

import noobbot.messages.TrackPiece;

/**
 * Created by gblike on 4/17/14.
 */
public class RadiusTrackPiece extends TrackPiece {
    public final float radius;
    public final float angle;
    public RadiusTrackPiece(final float radius, final float angle, final boolean hasSwitch) {
        super((float) Math.toRadians(angle) * radius, hasSwitch);
        this.radius = radius;
        this.angle = angle;
    }

    @Override
    public float getLengthInLane(Lane lane) {
        return (float) Math.toRadians(angle) * (radius + lane.distanceFromCenter);
    }
}
