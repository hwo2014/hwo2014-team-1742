package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class BotId {
    public final String name;
    public final String key;
    public BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}
