package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class CarDimensions {
    public final float length;
    public final float width;
    public final float guideFlagPosition;

    public CarDimensions(final float length, final float width, final float guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

}
