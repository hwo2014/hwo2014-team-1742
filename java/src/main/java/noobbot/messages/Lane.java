package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class Lane {
    public final int distanceFromCenter;
    public final int index; // seems redundant to have this index, but it's
                            // part of the message so we'll keep it for now
    public Lane(final int distanceFromCenter, final int index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }
}
