package noobbot.messages;

public class PositionMessage {
    public final String msgType;
    public final PositionData[] data;
    public final String gameID;
    public final int gameTick;

    public PositionMessage(final String msgType, final PositionData[] data, final String gameID, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameID = gameID;
        this.gameTick = gameTick;
    }
}
