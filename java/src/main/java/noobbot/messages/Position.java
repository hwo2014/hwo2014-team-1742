package noobbot.messages;

public class Position {
    public final int pieceIndex;
    public final double inPieceDistance;
    public final LaneInfo lane;

    public Position(final int pieceIndex, final double inPieceDistance, final LaneInfo lane) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
    }

    @Override
    public String toString() {
        return "index: " + pieceIndex + ", in-distance: " + inPieceDistance + ", lane: {" + lane.toString() + "}";
    }
}