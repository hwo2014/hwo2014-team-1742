package noobbot.messages;

/**
 * Created by gblike on 4/16/14.
 */
public class GameInit extends Message{
    public final Race race;
    public GameInit(final Race race) {
        this.race = race;
    }
}
