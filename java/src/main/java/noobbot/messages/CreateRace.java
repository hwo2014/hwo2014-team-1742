package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class CreateRace extends JoinRace {
    public CreateRace(final BotId botId, final String trackName, final String password, final int carCount) {
        super(botId, trackName, password, carCount);
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}
