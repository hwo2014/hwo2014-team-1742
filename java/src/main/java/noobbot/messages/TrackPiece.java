package noobbot.messages;

/**
 * Created by gblike on 4/16/14.
 */
public class TrackPiece {
    public final float length;
    public final boolean hasSwitch;

    public TrackPiece(final float length, final boolean hasSwitch) {
        this.length = length;
        this.hasSwitch = hasSwitch;
    }

    public float getLengthInLane(Lane lane) {
        return length;
    }
}
