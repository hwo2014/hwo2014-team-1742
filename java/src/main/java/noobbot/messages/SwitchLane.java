package noobbot.messages;

/**
 * Created by drzewiec on 4/18/2014.
 */
public class SwitchLane extends SendMsg {
    public final String direction;

    public SwitchLane(final String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
