package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class Car {
    public final CarID id;
    public final CarDimensions dimensions;
    public Car(final CarID id, final CarDimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }
}
