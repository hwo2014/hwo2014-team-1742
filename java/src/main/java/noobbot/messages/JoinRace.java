package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class JoinRace extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    public JoinRace(final BotId botId, final String trackName, final String password, final int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
