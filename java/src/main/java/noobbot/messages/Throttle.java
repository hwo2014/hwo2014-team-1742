package noobbot.messages;

public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

    public void increase(double delta) {
        if (value + delta > 1.0) value = 1.0;
        else value += delta;
    }

    public void decrease(double delta) {
        if (value - delta < 0.0) value = 0.0;
        else value -= delta;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        if (value > 1.0) this.value = 1.0;
        else if (value < 0.0) this.value = 0.0;
        else this.value = value;
    }
}
