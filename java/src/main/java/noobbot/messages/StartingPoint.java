package noobbot.messages;

import java.awt.geom.Point2D;

/**
 * Created by gblike on 4/17/14.
 */
public class StartingPoint {
    public final Point2D.Float position;
    public final float angle;
    public StartingPoint(final Point2D.Float position, final float angle) {
        this.position = position;
        this.angle = angle;
    }
}
