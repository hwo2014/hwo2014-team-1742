package noobbot.messages;

/**
 * Created by gblike on 4/17/14.
 */
public class RaceSession {
    public final int laps;
    public final int maxLapTimeMs;
    public final boolean quickRace;
    public RaceSession(final int laps, final int maxLapTimeMs, final boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }
}
