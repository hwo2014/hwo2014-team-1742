package noobbot;

import noobbot.messages.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drzewiec on 4/17/2014.
 */
public class Racer {
    private CarID id;
    private Throttle throttle;
    private double velocity;
    private double angle;
    private RaceHistory history;

    private Track track;

    public Racer(CarID id) {
        this.id = id;
        throttle = new Throttle(0.5);

        //initialize the logbook
        history = new RaceHistory();
        history.add(new RaceLogEntry(0, new Position(0, 0.0, new LaneInfo(0,0))));

        velocity = 0.0;
    }

    public Throttle getThrottle() {
        return throttle;
    }

    public CarID getId() {
        return id;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public void updatePosition(Position newPosition, int gameTick) {
        history.add(new RaceLogEntry(gameTick, newPosition));
        velocity = calculateVelocity();
    }

    private double calculateVelocity() {
        double distanceDelta = 0.0;
        double timeDelta = history.last().gameTick - history.last(-2).gameTick;

        Position newPosition = history.last().position;
        Position oldPosition = history.last(-2).position;
        //Calculate distance
        if (oldPosition.pieceIndex != newPosition.pieceIndex) {

            //Remaining distance of last piece we knew we were in
            Lane lane = track.lanes.get(oldPosition.lane.startLaneIndex); // let's assume that we'll never change lanes for now
            distanceDelta = track.pieces.get(oldPosition.pieceIndex).getLengthInLane(lane) - oldPosition.inPieceDistance;

            //Distances of all the pieces in between then and now
            for (int i = oldPosition.pieceIndex + 1; i < newPosition.pieceIndex; i++) {
                distanceDelta += track.pieces.get(i).getLengthInLane(lane);
            }

            //Distance traveled so far in current piece
            distanceDelta += newPosition.inPieceDistance;
        } else {
            distanceDelta = newPosition.inPieceDistance - oldPosition.inPieceDistance;
        }
        return distanceDelta / timeDelta;
    }
    public double getVelocity() {
        return velocity;
    }

    /**
     * Returns a racer initialized with the info in yourCar, if
     * available, otherwise uses name from join and default color of
     * red.
     *
     * @param yourCar car that the racer will control
     * @param join initial join parameters for default name
     * @return new Racer to control yourCar
     */
    public static Racer makeRacer(CarID yourCar, Join join) {
        // if name is null, default to name from join and 'red' as
        // color
        if (yourCar.name == null) {
            return new Racer(new CarID(join.name, "red"));
        }
        else {
            return new Racer(yourCar);
        }
    }
}
