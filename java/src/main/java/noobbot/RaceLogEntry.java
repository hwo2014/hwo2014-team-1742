package noobbot;

import noobbot.messages.Position;

/**
 * Created by gblike on 4/20/14.
 */
public class RaceLogEntry {
    public final int gameTick;
    public final Position position;

    public RaceLogEntry(final int gameTick, final Position position) {
        this.gameTick = gameTick;
        this.position = position;
    }
}
