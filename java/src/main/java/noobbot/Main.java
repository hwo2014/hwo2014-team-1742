package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

import noobbot.messages.*;
import noobbot.messages.serialization.*;
import org.apache.logging.log4j.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {
    private double throttle;
    static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = HelloWordOpenGson.MakeGson();

    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        Track track = null;
        Racer llama = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                PositionMessage msg = gson.fromJson(line, PositionMessage.class);

                //Find the llama in the list of position data
                List<PositionData> posList = Arrays.asList(msg.data);
                final CarID llamaID = llama.getId();
                PositionData llamaData = posList.stream().filter((PositionData data) -> data.id.equals(llamaID)).findFirst().get();

                llama.updatePosition(llamaData.piecePosition, msg.gameTick);
                log.debug("Tick {}, throttle: {}, velocity: {}, angle: {}", msg.gameTick, llama.getThrottle().getValue(), llama.getVelocity(), llamaData.angle);

                int index = llamaData.piecePosition.pieceIndex;
                List<TrackPiece> nextThree = track.nextNPieces(llamaData.piecePosition.pieceIndex, 1);
                if (llama.getVelocity() >= 8.0 && nextThree.stream().filter((TrackPiece piece) -> piece instanceof RadiusTrackPiece).count() > 0) {
                    llama.getThrottle().setValue(0.3);
                    log.debug("Whoa there boy! Setting throttle to {}", llama.getThrottle().getValue());
                } else if (llama.getVelocity() >= 5.0 && nextThree.stream().filter((TrackPiece piece) -> piece instanceof RadiusTrackPiece).count() > 0) {
                    llama.getThrottle().setValue(0.6);
                    log.debug("Whoa there boy! Setting throttle to {}", llama.getThrottle().getValue());
                } else if (llama.getThrottle().getValue() < 1.0) {
                    //Whip the llama's ass
                    llama.getThrottle().increase(0.1);
                    log.debug("Increasing throttle to {}", llama.getThrottle().getValue());
                }

                if (nextThree.stream().filter((TrackPiece piece) -> piece instanceof RadiusTrackPiece).count() > 0) {
                    RadiusTrackPiece piece = (RadiusTrackPiece)nextThree.stream().filter((TrackPiece tPiece) -> tPiece instanceof RadiusTrackPiece).findAny().get();
                    if (piece.angle < 0.0) {
                        SwitchLane msgToSend = new SwitchLane("Right");
                        send(msgToSend);
                    } else if (piece.angle > 0.0) {
                        SwitchLane msgToSend = new SwitchLane("Left");
                        send(msgToSend);
                    }
                }

                send(llama.getThrottle());
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                GameInit init = (GameInit)msgFromServer.data;
                track = init.race.track;
                llama.setTrack(track);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("crash")) {
                log.debug("CRASHU!!");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                log.debug("Received car info");
                llama = Racer.makeRacer((CarID) msgFromServer.data, join);
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
