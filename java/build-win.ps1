. ../config-win.ps1

if ($build_mode -eq "offline") {
    echo "offline build"
    mvn -B -o package -DskipTests
} else {
    mvn -B package -DskipTests
}